# script for preparing the 4 files :
# spam_bookmark , spam_tas, spam_user_data, spam_user_info for training
# The distribution of spamer to non spamer is about 70% spamer 30% non spammer.

# Output of this script is one file for non spammers one for spammers
# the files are csv with this format: BOOKMARK, TAGS , USER_URL

from asd import telegram_send
from asd import telegram_run
from asd import telegram_stop
import csv

# KEY = USER_ID ; VALUE = URL
user_url_dict = dict()
# KEY = CONTENT_ID ; VALUE = TAG
tag_dict = dict()
user_info_dict = dict()

user_url_file = '/scratch/deep_spam/data/bibsonomy/spam_user_data.csv'
tag_file = '/scratch/deep_spam/data/bibsonomy/spam_tas.csv'
user_info_file = '/scratch/deep_spam/data/bibsonomy/spam_user_info.csv'
bookmarks_file = '/scratch/deep_spam/data/bibsonomy/spam_bookmark.csv'

output_pos = '../data/raw/pos.csv'
output_neg = '../data/raw/neg.csv'


# user_not_found = 0
# user_url_file = '../data/test/urls.csv'
# tag_file = '../data/test/tags.csv'
# user_info_file = '../data/test/users.csv'
# bookmarks_file = '../data/test/bookmarks.csv'


def fill_url_dict():
    with open(user_url_file, 'r') as f:
        reader = csv.reader(f)
        for line in reader:
            if len(line) > 1:
                user_url_dict[line[0]] = line[1]


def fill_tag_dict():
    with open(tag_file, 'r') as f:
        reader = csv.reader(f)
        for line in reader:
            if line[0] in tag_dict:
                tag_dict[line[0]].append(line[1])
            else:
                tag_dict[line[0]] = [line[1]]


def fill_user_dict():
    with open(user_info_file, 'r') as f:
        reader = csv.reader(f)
        for line in reader:
            if len(line) == 3:
                user_info_dict[line[0]] = [line[1], line[2]]


def iterate_bookmarks():
    with open(bookmarks_file, 'r') as bookmarks, \
            open(output_pos, 'a+') as pos, \
            open(output_neg, 'a+') as neg:
        reader = csv.reader(bookmarks)
        for line in reader:
            if len(line) > 5:
                user_id = line[3]
                if user_id in user_info_dict:
                    if (user_info_dict[user_id])[0] == '1':
                        bookmark = "\"" + line[2] + "\""
                        content_id = line[0]
                        tags = []
                        if content_id in tag_dict:
                            tags = tag_dict[content_id]
                        else:
                            print("tag not found")
                        user_url = "\"\""
                        if user_id in user_url_dict :
                            user_url = "\"" + user_url_dict[user_id] + "\""
                        spam = (user_info_dict[user_id])[1] == '1'
                        if spam:
                            pos.write(bookmark + ",\"" + ", ".join(str(t) for t in tags) + "\"," + user_url)
                            pos.write("\n")
                        else:
                            neg.write(bookmark + ",\"" + ", ".join(str(t) for t in tags) + "\"," + user_url)
                            neg.write("\n")
                else:
                    print("User not found id:", user_id)
                    # user_not_found = user_not_found + 1
            else:
                print("failed to parse bookmark.")


if __name__ == '__main__':
    # telegram_run()
    telegram_send("start fill url dict")
    print("start fill url dict")
    fill_url_dict()
    print("done")
    telegram_send("done")
    telegram_send("start fill tag dict")
    print("start fill tag dict")
    fill_tag_dict()
    print("done")
    telegram_send("done")
    telegram_send("start fill user dict")
    print("start fill user dict")
    fill_user_dict()
    print("done")
    telegram_send("done")
    print("done")
    telegram_send("start iterating over bookmarks")
    print("start iterating over bookmarks")
    iterate_bookmarks()
    telegram_send("done")
    print("done")
    # print("Users not found:", user_not_found)
    # telegram_stop()
