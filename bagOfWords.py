import nltk
from nltk.tokenize import RegexpTokenizer
from nltk.stem import WordNetLemmatizer
import numpy as np
import random
import itertools
import pickle
from collections import Counter
from asd import telegram_send
from asd import telegram_run
from asd import telegram_stop
import os

print("Download nltk stuff")
nltk.download('punkt')
nltk.download('wordnet')
print("done")

lemmatizer = WordNetLemmatizer()
rt = RegexpTokenizer(r'[^\W_]+|[^\W_\s]+')
num_blobs = 10


# works with csv and txt
def create_lexicon(pos, neg):
    words = []
    for file in [pos, neg]:
        with open(file, 'r') as f:
            for line in f:
                all_words = rt.tokenize(line.lower())
                words += list(all_words)
    words = [lemmatizer.lemmatize(i) for i in words]
    w_counts = Counter(words)
    # lexicon with
    lexicon = set()
    minimum_occurrences = 3000
    telegram_send("Starting with min occ = " + str(minimum_occurrences))
    telegram_send("lexicon size = 0")
    while (len(lexicon) < 1000) & (minimum_occurrences >= 0):
        for w in w_counts:
            if w_counts[w] >= minimum_occurrences:
                lexicon.add(w)
        minimum_occurrences -= 20
    telegram_send("min occ now = " + str(minimum_occurrences))
    telegram_send("lexicon size = " + str(len(lexicon)))
    return list(lexicon)


def sample_handling(sample, lexicon, classification, start, stop):
    feature_set = []
    with open(sample, 'r') as file:
        for line in itertools.islice(file, start, stop):
            current_words = rt.tokenize(line.lower())
            current_words = [lemmatizer.lemmatize(i) for i in current_words]
            features = np.zeros(len(lexicon))
            for word in current_words:
                if word.lower() in lexicon:
                    index_value = lexicon.index(word.lower())
                    features[index_value] += 1
            features = list(features)
            feature_set.append([features, classification])

    return feature_set


def toFeatures(csv_line,lexicon):
    # the csv_line looks like: "bookmarkurl","tag1,tag2,tag3","user_url"
    current_words = rt.tokenize(csv_line.lower())
    current_words = [lemmatizer.lemmatize(i) for i in current_words]
    features = np.zeros(len(lexicon))
    for word in current_words:
        if word.lower() in lexicon:
            index_value = lexicon.index(word.lower())
            features[index_value] += 1
        features = np.array(features)
    return features


def create_features(pos, neg, lexicon, start, stop):

    features = []
    features += sample_handling(pos, lexicon, [1, 0], start, stop)
    features += sample_handling(neg, lexicon, [0, 1], start, stop)

    random.shuffle(features)
    features = np.array(features)

    x = list(features[:, 0])
    y = list(features[:, 1])

    return x, y


if __name__ == '__main__':

    pos = '../data/raw/pos.txt'
    neg = '../data/raw/neg.txt'

    # start_telegram_bot
    telegram_run()
    print("try to create lexicon")
    telegram_send("create lexicon")
    lexicon = create_lexicon(pos, neg)
    print("Print lexicon")
    telegram_send("print lexicon")
    try:
        os.remove('lexicon.txt')
    except OSError:
        pass
    lexicon_file = open('lexicon.txt', "a+")
    print("Lexicon size:", len(lexicon))
    for word in lexicon:
        lexicon_file.write(word)
        lexicon_file.write("\n")

    lexicon_file.close()
    telegram_send("done")

    num_pos = sum(1 for line in open(pos))-1
    num_neg = sum(1 for line in open(neg))-1
    print("pos",num_pos,"neg",num_neg)
    telegram_send("pos: "+str(pos)+ " neg:" + str(neg))
    smallest = (num_neg if (num_pos > num_neg) else num_pos);

    print("Number of lines we work on:" + str(smallest))
    for i in range(1,num_blobs+1):
        start = int((i-1)*(smallest/num_blobs))
        stop =  int(i*(smallest/num_blobs))
        print("create features from line ", start, "to line", stop)
        telegram_send("create blob"+str(i))
        x,y = create_features(pos,neg, lexicon, start, stop)

        with open("../data/preprocessed_bow/preprocessed" +str(i)+ ".pickle", 'wb') as f:
            pickle.dump([x,y], f)

    telegram_stop()
