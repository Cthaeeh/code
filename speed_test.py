from tensorpack import *
from MyDataFlow import MyDataFlow
from bagOfWords import toFeatures

lexicon = ('porn','paper')

if __name__ == '__main__':
    ds = MyDataFlow('train', shuffle=True)
    ds = LocallyShuffleData(ds,100)
    ds = MapData(ds, lambda dp: [toFeatures(dp[0], lexicon), dp[1]])
    #ds = BatchData(ds,128)
    ds = PrefetchDataZMQ(ds, nr_proc=5)

    test = TestDataSpeed(ds, size=10000)
    test.start_test()
