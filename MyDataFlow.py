from tensorpack import RNGDataFlow
import numpy as np


# The source files are two csv files at data/raw/(pos or neg).csv
# These dont have the same size. There is much more spam than non-spam

class MyDataFlow(RNGDataFlow):
    # test_size = 10
    # pos_file = "../data/raw/pos.csv"  # about 7 mio entries
    # neg_file = "../data/raw/neg.csv"  # about 400k entries

    pos_file = "/home/stud/hofmann/data/raw/pos.csv"  # about 7 mio entries
    neg_file = "/home/stud/hofmann/data/raw/neg.csv"  # about 400k entries
    test_size = 5000

    def __init__(self, train_or_test, shuffle=True):
        assert train_or_test in ['train', 'test']
        self.train_or_test = train_or_test
        self.shuffle = shuffle

    def get_data(self):
        with open(self.pos_file) as pos_f, open(self.neg_file) as neg_f:
            pos_lines = pos_f.readlines()
            neg_lines = neg_f.readlines()

            if self.train_or_test == 'train':
                pos_indexes = list(range(self.test_size, len(pos_lines)))
                neg_indexes = list(range(self.test_size, len(neg_lines)))
            else:
                pos_indexes = list(range(0, self.test_size))
                neg_indexes = list(range(0, self.test_size))

            if self.shuffle:
                self.rng.shuffle(pos_indexes)
                self.rng.shuffle(neg_indexes)

                # zip should stop when end of shorter list is reached.
            for pos, neg in zip(pos_indexes, neg_indexes):
                yield [pos_lines[pos], np.array([1, 0])]
                yield [neg_lines[neg], np.array([0, 1])]

    def size(self):
        if self.train_or_test == 'test':
            return self.test_size
        else:
            num_lines_neg = sum(1 for line in open(self.neg_file))
            num_lines_pos = sum(1 for line in open(self.pos_file))
            if (num_lines_neg > num_lines_pos):
                return num_lines_neg
            else:
                return num_lines_pos
