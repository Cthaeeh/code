import os
import argparse
import tensorflow as tf
from MyDataFlow import MyDataFlow
from char_processing import to_features

from tensorpack import *

BATCH_SIZE = 128

CLASSES = 2

LEXICON = ('a', 'b', 'c', 'd', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
           'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
           '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
           '-', ',', ';', '.', '!', '?', ':', '/', '\\', '|', '_', '@',
           '#', '$', '%', '^', '&', '*', '+', '=', '<', '>', '(', ')',
           '[', ']', '{', '}'
           )

NUM_CHARS = 100


class Model(ModelDesc):
    def inputs(self):
        return [tf.placeholder(tf.float16, [None, len(LEXICON) * NUM_CHARS], 'input'),
                tf.placeholder(tf.float32, [None, 2], 'label')]

    def build_graph(self, input, label):
        input_layer = tf.reshape(input, [BATCH_SIZE, len(LEXICON) * NUM_CHARS, 1])

        print(input_layer.get_shape())

        conv1 = tf.layers.conv1d(
            inputs=input_layer,
            data_format="channels_last",
            filters=256,
            kernel_size=7 * (len(LEXICON)),  # always look at 7 chars at one time
            padding="same",  # Okay but what does "same" mean
            activation=tf.nn.relu,
            strides=len(LEXICON)  # always move by one char over the text.
        )

        print("after conv1", conv1.get_shape())

        pool1 = tf.layers.max_pooling1d(
            inputs=conv1,
            pool_size=3,
            strides=3
        )

        print("after pool1", pool1.get_shape())

        conv2 = tf.layers.conv1d(
            inputs=pool1,
            filters=256,
            kernel_size=7,
            padding="same",  # Okay but what does "same" mean
            activation=tf.nn.relu,
            strides=1
        )

        print("after conv2", conv2.get_shape())

        pool2 = tf.layers.max_pooling1d(
            inputs=conv2,
            pool_size=3,
            strides=3
        )

        print("after pool2", pool2.get_shape())

        conv3 = tf.layers.conv1d(
            inputs=pool2,
            filters=256,
            kernel_size=3,
            padding="same",  # Okay but what does "same" mean
            activation=tf.nn.relu,
            strides=1
        )

        print("after conv3", conv3.get_shape())

        conv4 = tf.layers.conv1d(
            inputs=conv3,
            filters=256,
            kernel_size=3,
            padding="same",  # Okay but what does "same" mean
            activation=tf.nn.relu,
            strides=1
        )

        print("after conv4", conv4.get_shape())

        conv5 = tf.layers.conv1d(
            inputs=conv4,
            filters=256,
            kernel_size=3,
            padding="same",  # Okay but what does "same" mean
            activation=tf.nn.relu,
            strides=1
        )

        print("after conv5", conv5.get_shape())

        conv6 = tf.layers.conv1d(
            inputs=conv5,
            filters=256,
            kernel_size=3,
            padding="same",  # Okay but what does "same" mean
            activation=tf.nn.relu,
            strides=1
        )

        print("after conv5", conv6.get_shape())

        pool6 = tf.layers.max_pooling1d(
            inputs=conv6,
            pool_size=3,
            strides=3
        )

        print("after pool6", pool6.get_shape())
        flat = tf.contrib.layers.flatten(pool6)

        print("after flat", flat.get_shape())
        dense1 = tf.layers.dense(inputs=flat, units=1024, activation=tf.nn.relu)

        print("after dense1", dense1.get_shape())

        dense2 = tf.layers.dense(inputs=dense1, units=1024, activation=tf.nn.relu)

        print("after dense2", dense2.get_shape())

        output = tf.layers.dense(inputs=dense2, units=2)

        print("after output", output.get_shape())
        print("label shape.", label.get_shape())

        cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(logits=output, labels=label, name=None))
        summary.add_moving_summary(cost)

        correct = tf.equal(tf.argmax(output, 1), tf.argmax(label, 0))
        accuracy = tf.reduce_mean(tf.cast(correct, 'float'), name='accuracy')
        summary.add_moving_summary(accuracy)

        return cost

    def optimizer(self):
        lr = tf.get_variable('learning_rate', initializer=5e-3, trainable=False)
        return tf.train.AdamOptimizer(lr)


def get_data():
    # start with lines that look like: "someBookmarkUrl","tag1,tag2","userUrl"
    ds_train = MyDataFlow('train', shuffle=True)
    # locally shuffle
    ds_train = LocallyShuffleData(ds_train, 100)

    ds_train = MapData(ds_train, lambda dp: [to_features(dp[0], LEXICON, NUM_CHARS), dp[1]])
    ds_train = PrefetchDataZMQ(ds_train, nr_proc=10)

    ds_train = BatchData(ds_train, BATCH_SIZE)

    ds_test = MyDataFlow('test', shuffle=False)
    ds_test = MapData(ds_test, lambda dp: [to_features(dp[0], LEXICON, NUM_CHARS), dp[1]])
    ds_test = BatchData(ds_test, BATCH_SIZE)

    return ds_train, ds_test


def get_config():
    logger.set_logger_dir('../results/tensorpack/')

    print("get data")
    ds_train, ds_test = get_data()
    print("done")

    return TrainConfig(
        model=Model(),
        dataflow=ds_train,
        callbacks=[
            ModelSaver(),
            MaxSaver('validation_accuracy'),
            InferenceRunner(ds_test, [ScalarStats('accuracy')]),
        ],
        steps_per_epoch=ds_train.size(),
        max_epoch=10,
    )


if __name__ == '__main__':
    print("go to tensor_pack_main")
    parser = argparse.ArgumentParser()
    parser.add_argument('--gpu', help='comma separated list of GPU(s) to use.')
    parser.add_argument('--load', help='load model')
    args = parser.parse_args()

    if args.gpu:
        os.environ['CUDA_VISIBLE_DEVICES'] = args.gpu

    print("get config")
    config = get_config()

    if args.load:
        config.session_init = SaverRestore(args.load)

    print("run training")
    launch_train_with_config(config, SimpleTrainer())
