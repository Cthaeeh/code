import tensorflow as tf
import pickle
import numpy as np
from asd import telegram_send
from asd import telegram_run
from asd import telegram_stop

# use last pickle for testing
data = pickle.load(open('../data/preprocessed_bow/preprocessed10.pickle', 'rb'))
test_x = data[0]
test_y = data[1]

nodesLayer1 = 500
nodesLayer2 = 500
nodesLayer3 = 500

classes = 2

batchSize = 100

x = tf.placeholder('float', [None, len(test_x[0])])
y = tf.placeholder('float')


def modelNN(data):
    hidden_layer1 = {'weights': tf.Variable(tf.random_normal([len(test_x[0]), nodesLayer1])),
                     'biases': tf.Variable(tf.random_normal([nodesLayer1]))}

    hidden_layer2 = {'weights': tf.Variable(tf.random_normal([nodesLayer1, nodesLayer2])),
                     'biases': tf.Variable(tf.random_normal([nodesLayer2]))}

    hidden_layer3 = {'weights': tf.Variable(tf.random_normal([nodesLayer2, nodesLayer3])),
                     'biases': tf.Variable(tf.random_normal([nodesLayer3]))}

    output_layer = {'weights': tf.Variable(tf.random_normal([nodesLayer3, classes])),
                    'biases': tf.Variable(tf.random_normal([classes]))}

    l1 = tf.add(tf.matmul(data, hidden_layer1['weights']), hidden_layer1['biases'])
    l1 = tf.nn.relu(l1)

    l2 = tf.add(tf.matmul(l1, hidden_layer2['weights']), hidden_layer2['biases'])
    l2 = tf.nn.relu(l2)

    l3 = tf.add(tf.matmul(l2, hidden_layer3['weights']), hidden_layer3['biases'])
    l3 = tf.nn.relu(l3)

    output = tf.add(tf.matmul(l3, output_layer['weights']), output_layer['biases'])

    return output


def train_neural_net(x, y):
    prediction = modelNN(x)
    cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=prediction, labels=y, name=None))
    optimizer = tf.train.AdamOptimizer().minimize(cost)

    epochs = 20

    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())

        for epoch in range(epochs):
            epoch_loss = 0
            # iterate over pickle, because on giant pickle is to big.
            for pickle_num in range(1, 9):
                pickle_data = pickle.load(open('../data/preprocessed_bow/preprocessed' + str(pickle_num) + '.pickle', 'rb'))
                train_x = pickle_data[0]
                train_y = pickle_data[1]
                i = 0
                while i < len(train_x):
                    start = i
                    end = i + batchSize

                    batch_x = np.array(train_x[start:end])
                    batch_y = np.array(train_y[start:end])
                    _, c = sess.run([optimizer, cost], feed_dict={x: batch_x, y: batch_y})
                    epoch_loss += c
                    i += batchSize
            print('Epoch ', epoch, ' completed out of ', epochs, ' loss: ', epoch_loss)
            telegram_send('Epoch ' + str(epoch) +
                          ' completed out of ' + str(epochs) +
                          ' loss: ' + str(epoch_loss))

        correct = tf.equal(tf.argmax(prediction, 1), tf.argmax(y, 1))
        accuracy = tf.reduce_mean(tf.cast(correct, 'float'))
        print("test_samples:", len(test_x))
        accuracy = accuracy.eval({x: test_x, y: test_y})
        print(' accuracy: ', accuracy)
        telegram_send("accuracy: " + str(accuracy) + " %")


if __name__ == '__main__':
    telegram_run()
    train_neural_net(x, y)
    telegram_stop()
