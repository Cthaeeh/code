from char_processing import to_features

if __name__ == '__main__':
    string = "abx"
    lexicon = ('a', 'b', 'c')
    LENGTH = 4
    print(to_features(string, lexicon, LENGTH))
