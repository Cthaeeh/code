import os
import argparse
import tensorflow as tf
from MyDataFlow import MyDataFlow
from bagOfWords import create_lexicon
from bagOfWords import toFeatures

from tensorpack import *

"""
This is a boiler-plate template.
All code is in this file is the most minimalistic way to solve a deep-learning problem with cross-validation.
"""

BATCH_SIZE = 128
NODES_LAYER_1 = 500
NODES_LAYER_2 = 500
NODES_LAYER_3 = 500

CLASSES = 2

# lexicon = create_lexicon("../data/raw/pos.csv", "../data/raw/neg.csv")
lexicon = ('porn', 'paper')


class Model(ModelDesc):
    def inputs(self):
        return [tf.placeholder(tf.float32, [None, len(lexicon)], 'input'),
                tf.placeholder(tf.float32, [None, 2], 'label')]

    def build_graph(self, bow_input, label):
        hidden_layer1 = {'weights': tf.get_variable('l1', shape=[len(lexicon), NODES_LAYER_1],
                                                    initializer=tf.random_normal_initializer),
                         'biases': tf.get_variable('b1', shape=[NODES_LAYER_1],
                                                   initializer=tf.random_normal_initializer)}

        hidden_layer2 = {'weights': tf.get_variable('l2', shape=[NODES_LAYER_1, NODES_LAYER_2],
                                                    initializer=tf.random_normal_initializer),
                         'biases': tf.get_variable('b2', shape=[NODES_LAYER_2],
                                                   initializer=tf.random_normal_initializer)}

        hidden_layer3 = {'weights': tf.get_variable('l3', shape=[NODES_LAYER_2, NODES_LAYER_3],
                                                    initializer=tf.random_normal_initializer),
                         'biases': tf.get_variable('b3', shape=[NODES_LAYER_3],
                                                   initializer=tf.random_normal_initializer)}

        output_layer = {'weights': tf.get_variable('l4', shape=[NODES_LAYER_3, CLASSES],
                                                   initializer=tf.random_normal_initializer),
                        'biases': tf.get_variable('b4', shape=[CLASSES],
                                                  initializer=tf.random_normal_initializer)}

        # hidden_layer2 = {'weights': tf.get_variable(tf.random_normal([NODES_LAYER_1, NODES_LAYER_2])),
        #                   'biases': tf.get_variable(tf.random_normal([NODES_LAYER_2]))}

        #  hidden_layer3 = {'weights': tf.get_variable(tf.random_normal([NODES_LAYER_2, NODES_LAYER_3])),
        #                   'biases': tf.get_variable(tf.random_normal([NODES_LAYER_3]))}

        #  output_layer = {'weights': tf.get_variable(tf.random_normal([NODES_LAYER_3, CLASSES])),
        #                  'biases': tf.get_variable(tf.random_normal([CLASSES]))}

        l1 = tf.add(tf.matmul(bow_input, hidden_layer1['weights']), hidden_layer1['biases'])
        l1 = tf.nn.relu(l1)

        l2 = tf.add(tf.matmul(l1, hidden_layer2['weights']), hidden_layer2['biases'])
        l2 = tf.nn.relu(l2)

        l3 = tf.add(tf.matmul(l2, hidden_layer3['weights']), hidden_layer3['biases'])
        l3 = tf.nn.relu(l3)

        output = tf.add(tf.matmul(l3, output_layer['weights']), output_layer['biases'])

        cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(logits=output, labels=label, name=None))
        summary.add_moving_summary(cost)

        correct = tf.equal(tf.argmax(output, 1), tf.argmax(label, 1))
        accuracy = tf.reduce_mean(tf.cast(correct, 'float'), name='accuracy')
        summary.add_moving_summary(accuracy)

        return cost

    def optimizer(self):
        lr = tf.get_variable('learning_rate', initializer=5e-3, trainable=False)
        return tf.train.AdamOptimizer(lr)


def get_data():
    # start with lines that look like: "someBookmarkUrl","tag1,tag2","userUrl"
    ds_train = MyDataFlow('train', shuffle=True)
    # locally shuffle
    ds_train = LocallyShuffleData(ds_train, 100)
    # convert to bow array, keep the label as it is
    ds_train = MapData(ds_train, lambda dp: [toFeatures(dp[0], lexicon), dp[1]])
    ds_train = PrefetchDataZMQ(ds_train, nr_proc=16)

    ds_train = BatchData(ds_train, BATCH_SIZE)

    ds_test = MyDataFlow('test', shuffle=False)
    # convert to bow array, keep the label as it is
    ds_test = MapData(ds_test, lambda dp: [toFeatures(dp[0], lexicon), dp[1]])
    ds_test = BatchData(ds_test, BATCH_SIZE)

    return ds_train, ds_test


def get_config():
    logger.set_logger_dir('../results/tensorpack/')

    print("get data")
    ds_train, ds_test = get_data()
    print("done")

    return TrainConfig(
        model=Model(),
        dataflow=ds_train,
        callbacks=[
            ModelSaver(),
            MaxSaver('validation_accuracy'),
            InferenceRunner(ds_test, [ScalarStats('accuracy')]),
        ],
        steps_per_epoch=ds_train.size(),
        max_epoch=100,
    )


if __name__ == '__main__':
    print("go to tensor_pack_main")
    parser = argparse.ArgumentParser()
    parser.add_argument('--gpu', help='comma separated list of GPU(s) to use.')
    parser.add_argument('--load', help='load model')
    args = parser.parse_args()

    if args.gpu:
        os.environ['CUDA_VISIBLE_DEVICES'] = args.gpu

    print("get config")
    config = get_config()

    if args.load:
        config.session_init = SaverRestore(args.load)

    print("run training")
    launch_train_with_config(config, SimpleTrainer())
