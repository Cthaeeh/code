import numpy as np


# the csv_line might look like: "someMaliciousUrl.com","evil_tag1,evil_tag2,"user_url"
# the lexicon might look like (a,b,c,d,...,:,;...)
# this function turns the string into an array of one-hot encoded chars.
def to_features(csv_line, lexicon, num_chars):
    lex_length = len(lexicon)
    features = np.zeros(lex_length * num_chars, dtype=int)

    # iterate over chars
    for i, c in enumerate(csv_line):
        if c in lexicon :
            lex_index = lexicon.index(c)
            feature_index = i * lex_length + lex_index
            if feature_index < (lex_length * num_chars):
                features[feature_index] = 1

    return features

