from telegram.ext import Updater
from telegram.ext import CommandHandler
from telegram.ext.dispatcher import run_async
import time

chat_id = 0
updater = 0



def start(bot, update):
    update.message.reply_text("I'm a bot, Nice to meet you!")


def subscribe(bot, update):
    update.message.reply_text("You subscribed to updates from cthaeth_bot")
    global chat_id
    chat_id = update.message.chat_id


def telegram_send(message):
    if chat_id != 0:
        try:
            updater.bot.send_message(chat_id=chat_id, text=message)
        except:
            print("send fail")
    else:
        print("chat id 0")



def telegram_run():
    # Create Updater object and attach dispatcher to it
    global updater
    updater = Updater(token='560989310:AAGvg6GWta9DF_KwEUisF5fmoO9pHlL1qDI')
    dispatcher = updater.dispatcher
    print("Bot started","wait 5s")
    # Add command handler to dispatcher
    start_handler = CommandHandler('start', start)
    sub_handler = CommandHandler('sub', subscribe)
    dispatcher.add_handler(start_handler)
    dispatcher.add_handler(sub_handler)

    # Start the bot

    updater.start_polling()

    time.sleep(10)


def telegram_stop():
    global updater
    updater.idle()
